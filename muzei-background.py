#!/usr/bin/env python3

import requests
import datetime
import os
import dbus
import argparse
from pathlib import Path

curdir = os.path.dirname(os.path.realpath(__file__))
nextpath = curdir + '/nextTime.txt'
titlepath = curdir + '/title.txt'
bypath = curdir + '/by.txt'

with open(nextpath, 'r') as reader:
    next = reader.read()

present = datetime.datetime.now()
nextTime = datetime.datetime.strptime(next[:19], '%Y-%m-%dT%H:%M:%S') if next != '' else ''

def download():
    print('Processing the request...')
    url = 'https://muzeiapi.appspot.com/featured'
    params = (
        ('cachebust', '1'),
    )
    response = requests.get(url, params=params)

    uri = response.json()['imageUri']
    by = response.json()['byline']
    title = response.json()['title']
    next = response.json()['nextTime']

    filename = datetime.datetime.today().strftime('%Y-%m-%d') + '-' + title.replace(' ', '_') + '.jpg'
    filepath = curdir + '/images/' + filename

    print('Downloading "' + title + '" from ' + uri)

    response = requests.get(uri)

    with open(filepath, 'wb') as f:
        f.write(response.content)

    print('Setting background image...')


# taken from https://github.com/bharadwaj-raju/libdesktop/issues/1
# Thank you very much :)

    command = """
    qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript '
        var allDesktops = desktops();
        print (allDesktops);
        for (i=0;i<allDesktops.length;i++) {{
            d = allDesktops[i];
            d.wallpaperPlugin = "org.kde.image";
            d.currentConfigGroup = Array("Wallpaper",
                                         "org.kde.image",
                                         "General");
            d.writeConfig("Image", "file:///{path}")
         }}
    '
    """
    os.system(command.format(path=filepath))

    # set link to current image for screen locker
    current_image_path = Path(curdir + '/images/current.jpg')
    current_image_path.unlink(missing_ok = True)
    current_image_path.symlink_to(filepath)


    with open(nextpath, 'w') as f:
        f.write(next)

    with open(titlepath, 'w') as f:
        f.write(title)

    with open(bypath, 'w') as f:
        f.write(by)

    print('Done!')

if next != '' and nextTime < present:
    download()

elif next == "":
    print("This is either the first time you run this script or an error occured.\nResetting nextTime...")
    with open(nextpath, 'w') as f:
        f.write('1970-01-01T00:00:00+00:00')
    
    print("Restarting...")
    download()

